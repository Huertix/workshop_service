#!/usr/bin/env bash

set -eu -o pipefail

echo "Running Migrations...."
if ! pipenv run python manage.py migrate --settings=$DJANGO_SETTINGS_MODULE
then
  # This helps docker/k8s to retrying provisioning while DB is not yet ready
  echo "Migration failed"
  exit 1
fi

echo "Loading Initial DB data...."
if ! pipenv run ./manage.py loaddata /app/src/fixtures/stars.json --settings=$DJANGO_SETTINGS_MODULE
then
  # This helps docker/k8s to retrying provisioning while DB is not yet ready
  echo "Data loading failed"
  exit 1
fi


if [ "$APP_ENVIRONMENT" == "TEST" ]
then
    echo "Executing command: $@"
    exec pipenv run "$@"
else
    echo "Starting App Server..."
    pipenv run newrelic-admin run-program gunicorn --chdir /app/catalog --preload --reuse-port --max-requests 10000 -b 0.0.0.0:5000 --log-level=info --log-file="-" --access-logfile="-" --error-logfile="-" -t 90 --capture-output --workers 2 catalog.wsgi
fi
