import json
import logging

from rest_framework import status

from src.models import Star
from rest_framework.decorators import api_view
from rest_framework.response import Response

from src.serializer import StarSerializer


logger = logging.getLogger("root")

@api_view(['GET'])
def stars_list(request):
    logger.info('All stars requested!')
    if request.method == 'GET':
        stars = Star.objects.all().order_by('name')
        serializer = StarSerializer(stars, many=True)
        logger.debug(f"{{stars: {json.dumps(serializer.data)}}}")
        return Response(serializer.data)


@api_view(['GET'])
def star_detail(request, name):
    logger.info(f'Stars {name} requested!')

    star = None
    try:
        star = Star.objects.get(name=name)
    except Star.DoesNotExist:
        logger.error(f'Star {name} not in DB!')
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = StarSerializer(star)
        return Response(serializer.data)
