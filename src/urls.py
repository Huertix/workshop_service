from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from src import views

urlpatterns = [
    path('stars/', views.stars_list),
    path('stars/<str:name>', views.star_detail),
]

urlpatterns = format_suffix_patterns(urlpatterns)
