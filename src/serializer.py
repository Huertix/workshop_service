from src.models import Star
from rest_framework import serializers


class StarSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Star
        fields = ['name', 'distance', 'link']
