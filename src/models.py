from django.db import models


class Star(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    distance = models.CharField(max_length=100)
    link = models.CharField(max_length=300)

