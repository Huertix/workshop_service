FROM python:3.7-slim

EXPOSE 5000

ENV PYTHONUNBUFFERED 1
ENV PIPENV_VENV_IN_PROJECT 1
ENV DJANGO_SETTINGS_MODULE catalog.settings


RUN apt-get update && \
    pip install pipenv && \
    apt-get clean

WORKDIR /app

COPY . /app


RUN pipenv install && \
    chmod 755 /app/entrypoint.sh

ENTRYPOINT [ "/app/entrypoint.sh" ]
